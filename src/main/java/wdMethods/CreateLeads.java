package wdMethods;


import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class CreateLeads extends SeMethods{
//@Test
	
public void leads()

{
	startApp("chrome", "http://leaftaps.com/opentaps/");
	WebElement element1 = locateElement("username");
	type(element1, "DemoSalesManager");
	WebElement pass1 = locateElement("password");
	type(pass1, "crmsfa");
	WebElement clk = locateElement("class", "decorativeSubmit");
	click(clk);
	WebElement lt1 = locateElement("linktext", "CRM/SFA");
	click(lt1);
	WebElement lt2 = locateElement("linktext", "Create Lead");
	click(lt2);
	WebElement element2 = locateElement("createLeadForm_companyName");
	type(element2, "IBM");
	WebElement element3 = locateElement("createLeadForm_firstName");
	type(element3, "Anandha");
	WebElement element4 = locateElement("createLeadForm_lastName");
	type(element4, "Ganesh");
	WebElement element5 = locateElement("class", "smallSubmit");
	click(element5);
	verifyTitle("Leaftaps Login");
	
	
}
	
}
